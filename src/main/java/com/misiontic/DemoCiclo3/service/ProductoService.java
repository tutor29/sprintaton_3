package com.misiontic.DemoCiclo3.service;

import com.misiontic.DemoCiclo3.IntefaceService.IProductoService;
import com.misiontic.DemoCiclo3.interfaces.IProducto;
import com.misiontic.DemoCiclo3.model.Producto;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProductoService implements IProductoService{
    
    @Autowired
    private IProducto data;

    @Override
    public List<Producto> listar() {
        return (List<Producto>) data.findAll();
    }

    @Override
    public Optional<Producto> listarID(int id) {
        return data.findById(id);
    }

    @Override
    public Producto save(Producto producto) {
        return data.save(producto);
    }

    @Override
    public void delete(int id) {
        data.deleteById(id);
    }
}
