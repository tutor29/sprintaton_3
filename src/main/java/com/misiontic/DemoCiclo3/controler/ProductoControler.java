package com.misiontic.DemoCiclo3.controler;

import java.util.List;
import com.misiontic.DemoCiclo3.IntefaceService.IProductoService;
import com.misiontic.DemoCiclo3.model.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@CrossOrigin("*")
@RequestMapping("/productos")
public class ProductoControler {
    
    @Autowired
    private IProductoService service;
    
    @GetMapping("/listar")
    public String listar(Model model){
        List<Producto> productos = service.listar();
        model.addAttribute("productos", productos);
        return "listar_productos";
    }
    
}
