package com.misiontic.DemoCiclo3.interfaces;

import com.misiontic.DemoCiclo3.model.Producto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IProducto extends CrudRepository<Producto, Integer>{
    
}
