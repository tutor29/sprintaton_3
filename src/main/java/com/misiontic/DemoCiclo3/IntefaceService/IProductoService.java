
package com.misiontic.DemoCiclo3.IntefaceService;

import com.misiontic.DemoCiclo3.model.Producto;
import java.util.List;
import java.util.Optional;

public interface IProductoService {
    public List<Producto> listar();
    public Optional<Producto> listarID(int id);
    public Producto save(Producto producto);
    public void delete(int id);
}
