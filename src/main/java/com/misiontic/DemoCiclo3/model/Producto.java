package com.misiontic.DemoCiclo3.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="producto")
public class Producto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idProducto")
    private int idProducto;
    
    @Column(name="nombreProducto")
    private String nombreProducto;
    
    @Column(name="valorCompra")
    private float valorCompra;
    
    @Column(name="valorVenta")
    private float valorVenta;
    
    @Column(name="cantidad")
    private float cantidad;

    public Producto(int idProducto, String nombreProducto, float valorCompra, float valorVenta, float cantidad) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.valorCompra = valorCompra;
        this.valorVenta = valorVenta;
        this.cantidad = cantidad;
    }

    public Producto() {
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public float getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(float valorCompra) {
        this.valorCompra = valorCompra;
    }

    public float getValorVenta() {
        return valorVenta;
    }

    public void setValorVenta(float valorVenta) {
        this.valorVenta = valorVenta;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
    
    
    
}
